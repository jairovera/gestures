//
//  TapGestureViewController.swift
//  Gestures
//
//  Created by Jairo Vera on 17/1/18.
//  Copyright © 2018 Jairo Vera. All rights reserved.
//

import UIKit

class TapGestureViewController: UIViewController {

    @IBOutlet weak var touchesLabel: UILabel!
    @IBOutlet weak var tapsLabel: UILabel!
    
    @IBOutlet weak var customView: UIView!
    @IBOutlet weak var coordLabel: UILabel!
    var taps = 0
    
    
    var inicial = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        var touchesCount = touches.count
        let tap = touches.first
        let tapCount = tap?.tapCount
        
        touchesLabel.text = "\(touchesCount ?? 0)"
        tapsLabel.text = "\(tapCount ?? 0)"
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        
        let point = touch?.location(in: self.view)
        
        let x = point?.x
        let y = point?.y
        coordLabel.text = "x: \(x ?? 0), y:\(y ?? 0)"
        //print("x: \(x), y:\(y)")
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("termino")
    }
    
    @IBAction func tapGestureAction(_ sender: Any) {
        
        let action = sender as! UITapGestureRecognizer
        
        if inicial{
            customView.backgroundColor = .red
            inicial = !inicial
        }
        else{
            customView.backgroundColor = .yellow
            inicial = !inicial
        }
//
//        if(action.state == .ended){
//            taps+=1
//            tapsLabel.text="\(taps)"
//        }
        
    }
}
