//
//  RotationViewController.swift
//  Gestures
//
//  Created by Jairo Vera on 17/1/18.
//  Copyright © 2018 Jairo Vera. All rights reserved.
//

import UIKit

class RotationViewController: UIViewController {

    @IBOutlet weak var customView: UIView!
    
    var red_coord:Int = 0
    var green_coord:Int = 0
    var blue_coord:Int = 0
    var scale:Int = 2
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    func uiColor_normalize(_ red_input: Double, _ green_input: Double,_ blue_input: Double)->UIColor{
        let red_value = CGFloat(red_input/255)
        let green_value = CGFloat(green_input/255)
        let blue_value = CGFloat(blue_input/255)
      
        let color = UIColor(red: red_value, green: green_value, blue: blue_value, alpha: 1)
        return color
    }
    
    func RGB_norm(_ input: CGFloat)->CGFloat{
        let output = Int(input) % 255
        return CGFloat(output)
    }
    
    
    func radians_to_degrees(input: CGFloat)->CGFloat{
        let output = (input * 180)/CGFloat.pi
        return output
    }
    
    func normalize_color(input: Int)->Int{
        if (input > 255){
            return 255
        }
        if(input < 0){
            return 0
        }
        
        return input
    }
    
    @IBAction func rotationAction(_ sender: UIRotationGestureRecognizer){
        
        
        let rotation = radians_to_degrees(input: sender.rotation)
        
        if rotation > 0 {
          
            red_coord += scale
            green_coord += scale
            blue_coord += scale
        }
        if rotation < 0  {
            
            red_coord -= scale
            green_coord -= scale
            blue_coord -= scale
            
        }
        red_coord = normalize_color(input: red_coord)
        green_coord = normalize_color(input: green_coord)
        blue_coord = normalize_color(input: blue_coord)
     
        customView.backgroundColor = uiColor_normalize(Double(red_coord), Double(green_coord), 100.0)
        
        
       
        
        
    }

}
