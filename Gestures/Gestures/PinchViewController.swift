//
//  PinchViewController.swift
//  Gestures
//
//  Created by Jairo Vera on 17/1/18.
//  Copyright © 2018 Jairo Vera. All rights reserved.
//

import UIKit

class PinchViewController: UIViewController {
    
    let screenSize:CGRect = UIScreen.main.bounds

    @IBOutlet weak var customView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print (screenSize)
        
        customView.frame = screenSize

        // Do any additional setup after loading the view.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func pinchAction(_ sender: UIPinchGestureRecognizer){
        
        let speed:CGFloat = CGFloat(1)
        let gestureRecognizer = sender
        
        if gestureRecognizer.velocity < speed && (gestureRecognizer.state == .began || gestureRecognizer.state == .changed) {
            
            gestureRecognizer.view?.transform = (gestureRecognizer.view?.transform.scaledBy(x: gestureRecognizer.scale, y: gestureRecognizer.scale))!
            gestureRecognizer.scale = 1.0
           
        }
        
        if gestureRecognizer.velocity > -speed && (gestureRecognizer.state == .began || gestureRecognizer.state == .changed) {
         
            gestureRecognizer.view?.transform = (gestureRecognizer.view?.transform.scaledBy(x: gestureRecognizer.scale, y: gestureRecognizer.scale))!
            gestureRecognizer.scale = 1.0
            
        }
        
        if gestureRecognizer.velocity > speed && (gestureRecognizer.state == .began || gestureRecognizer.state == .changed){
            customView.frame = screenSize
        }
        
        if gestureRecognizer.velocity < -speed && (gestureRecognizer.state == .began || gestureRecognizer.state == .changed){
            customView.frame = CGRect(x:screenSize.width/2-100, y:screenSize.height/2-100,width:200,height: 200)
            //customView.frame.size.height = screenSize.height/2
            //customView.frame.size.width = screenSize.width/2
           
        }
        
        if gestureRecognizer.state == .ended {
            print(gestureRecognizer.scale)
            print(gestureRecognizer.velocity)
        }
    }

}
