//
//  StoryboardsTabBarController.swift
//  Gestures
//
//  Created by Jairo Vera on 23/1/18.
//  Copyright © 2018 Jairo Vera. All rights reserved.
//

import UIKit

class StoryboardsTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        let left = tabBar.items![0]
        let right = tabBar.items![1]
        
        left.title = "Tap"
        right.title = "Gestures"
        
        left.image = #imageLiteral(resourceName: "ic_videocam")
        right.image = #imageLiteral(resourceName: "ic_explore")
        
    }

}
