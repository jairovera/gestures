//
//  SwipeViewController.swift
//  Gestures
//
//  Created by Jairo Vera on 17/1/18.
//  Copyright © 2018 Jairo Vera. All rights reserved.
//

import UIKit

class SwipeViewController: UIViewController {

    @IBOutlet weak var customView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func swipeUpAction(_ sender: Any){
        let action = sender as! UISwipeGestureRecognizer
        print(action.direction)
        customView.backgroundColor = .black
    }
    @IBAction func swipeDownAction(_ sender: Any){
        customView.backgroundColor = .blue
    }
    @IBAction func swipeLeftAction(_ sender: Any){
        customView.backgroundColor = .red
    }
    @IBAction func swipeRightAction(_ sender: Any){
        customView.backgroundColor = .yellow
    }

}
